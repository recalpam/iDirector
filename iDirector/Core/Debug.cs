﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using GTA;
using GTA.Native;

namespace iDirector.Core
{
	static class Debug {
		public enum Type {
			Info,
			Warning,
			Fatal,
			Debug
		}

		public static void Ingame( string message, Type type = Type.Info ) {
			GTA.UI.Notify( string.Format( "iDirector {0}: {1}", Enum.GetName( typeof( Type ), type) , message ) );
		}

		public static void Log( string message, Type type = Type.Info ) {
			File.AppendAllText( Settings.Path.sLog, string.Format( "{0} :: {1} - {2}{3}", System.DateTime.Now, Enum.GetName( typeof( Type ), type ), message, Environment.NewLine ) );
		}

		public static void Loud(string message, Type type = Type.Info ){
			Log( message, type );
			Ingame( message, type );
		}
	}
}
