﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Xml.Linq;
using GTA;
using GTA.Native;
using System.Diagnostics;

namespace iDirector.Core
{
	struct Internal
	{
		public const string sVersion	= "0.1a"; 
	}

	static class Settings
	{
		private struct Config
		{
			public const string sDirMod			= "iDirector";			// iDirector config directory name
			public const string sFileSettings	= "Settings.xml";			// iDirector settings file name
			public const string sFileLog		= "iDirectorLog.log";	// iDirector log file name
		}

		public struct Path
		{
			public static string sDir;
			public static string sSettings;
			public static string sLog;
		}

		public static XDocument xSettings;

		public static XContainer xHotkeys;

		public static bool Load() {

			// Format paths
			Path.sDir += string.Format( "{0}/", Config.sDirMod );
			Path.sSettings = Path.sDir + Config.sFileSettings;
			Path.sLog = Path.sDir + Config.sFileLog;

			// Basic checks
			if( !Directory.Exists( Path.sDir ) ) {
				Debug.Ingame( "Missing directory: " + Path.sDir, Debug.Type.Fatal );
				return false;
			}

			if( !File.Exists( Path.sSettings ) ) {
				Debug.Ingame( "Missing settings file: " + Path.sSettings, Debug.Type.Fatal );
				return false;
			}

			Debug.Log( "-----------" );
			Debug.Log( " iDirector Started." );
			Debug.Log( "-----------" );

			Debug.Log( "Parsing XDocument from settings file.." );
			xSettings = XDocument.Load( Path.sSettings );
			xHotkeys = xSettings.Element("Hotkeys");


			return true;
		}
	}
}
