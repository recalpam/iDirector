﻿using GTA;
using GTA.Native;
using GTA.Math;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using iDirector.Client;

namespace iDirector.Core
{
    public class Bootstrap : Script
    {
		public Bootstrap() {

			// init
			if( !iDirector.Core.Settings.Load() ) return;

			// UI
			Debug.Log( "Loading UI data.." );
			Client.UI.Handler.Load();

			// events
			Debug.Log( "Hooking events.." );
			Tick += Client.UI.Handler.onTick;
			Tick += Client.Network.onTick;
			KeyUp += Client.Input.onKeyUp;
			KeyDown += Client.Input.onKeyDown;

			
			// done
			Debug.Loud( "v" + Internal.sVersion + " successfuly initialized." );

		}

    }
}
