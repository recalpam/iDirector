﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GTA;
using GTA.Native;
using GTA.Math;
using iDirector.Core;

namespace iDirector.Client
{

	public static class Network
	{
		public struct Data
		{
			public static bool bOnline;
			public static List<GTA.Player> Players = new List<GTA.Player>();
			public static int iConnectedPlayers;
		}

		public static bool bPlayer(int handle, out Player player ) {
			player = new GTA.Player(handle);
			if( player.Handle == Game.Player.Handle  ||  !player.IsPlaying || player.Name.Contains( "invalid" ) || !player.Character.IsPlayer ) return false;
			return true;
		}

		public static void onTick(object caller, EventArgs e) {
			Data.Players.Clear();
			Data.bOnline = Function.Call<bool>( Hash.NETWORK_IS_IN_SESSION );
			if( !Data.bOnline ) return;

			Data.iConnectedPlayers = Function.Call<int>( Hash.NETWORK_GET_NUM_CONNECTED_PLAYERS );

			for( int i = 0;i < Data.iConnectedPlayers;i++ ) {
				Player player;
				if( !bPlayer( i, out player )) continue;
				
				Data.Players.Add( player );
			}

		}
	}
}
