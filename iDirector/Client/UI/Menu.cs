﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.Windows.Forms;
using GTA;
using GTA.Native;
using iDirector.Core;

namespace iDirector.Client.UI
{
	public class Menu:Widget
	{
		public struct Colors
		{
			public static Color frameBG = Color.FromArgb( 0, 0, 0, 0 );
			public static Color stateBG = Color.FromArgb( 75, 0, 0, 0 );


			public static Color titleText = Color.White;
			public static Color titleBG = Color.FromArgb( 200, 0, 0, 0 );
			public static Color titleBorder = Color.FromArgb( 150, 0, 0, 0 );



			public static Color itemIdleText = Color.White;
			public static Color itemIdleBG = Color.FromArgb( 35, 0, 0, 0 );
			public static Color itemActiveText = Color.Black;
			public static Color itemActiveBG = Color.White;
		}

		public Size Size = new Size( 400, 400 );



		private UIContainer Title( string text ) {
			var title = new UIContainer( new Point( 0, 0 ), Size.H( 75 ), Colors.titleBG );
			title.Items.Add( new GTA.UIText( text, new Point( Size.H( 30 ).Divide() ), 1.1f, Colors.titleText, GTA.Font.HouseScript, true ) );
			return title;
		}

		public State State( string name ) {
			var state = new State( name );
			state.Append( new UIContainer( new Point( 10, 85 ), Size.H( Size.Height - 75 ), Colors.stateBG ) );
			this.Append( state );
			return state;
		}

		public Menu() {

			// main container
			Container = new UIContainer( new Point( 10, 10 ), new Size( 400, 400 ), Colors.frameBG );

			// small borders on bottom & top of the container
			Container.Items.Add( new GTA.UIRectangle( new Point( 0, 0 ), Size.H( 5 ), Colors.titleBorder ) );
			Container.Items.Add( new GTA.UIRectangle( new Point( 0, 70 ), Size.H( 5 ), Colors.titleBorder ) );

			Container.Items.Add( Title( this.sName ) );
			if( null == stateCurrent && null != States ) {
				stateCurrent = States.First();
			}
		}

		public void Up() {
			//Debug.Loud(this.stateCurrent.List.itemAll.Count + "");
			this.stateCurrent.List.itemPrev();
		}
		public void Down() {
			this.stateCurrent.List.itemNext();
		}

		public void Select() {
			//Debug.Ingame( this.stateCurrent.List.itemCurrent.sName );
			this.stateCurrent.List.itemCurrent.Execute();
		}

		public void Back() {
			this.stateActivatePrevious();
		}

		public override void Draw() {
			base.Draw();
			stateCurrent.Draw();
		}


	}
	public static class Extensions
	{
		public static List List( this State state, string name ) {
			List list = new List( name, eItemChange );
			state.Append( ref list );
			state.List = list;
			return list;
		}

		public static void eItemChange( Item itemCurrent, Item itemNext ) {
			itemCurrent.Container.Color = Menu.Colors.itemIdleBG;
			itemCurrent.Container.Items.First().Color = Menu.Colors.itemIdleText;

			itemNext.Container.Color = Menu.Colors.itemActiveBG;
			itemNext.Container.Items.First().Color = Menu.Colors.itemActiveText;
		}

		public static List Item( this List list, string name, Item.ExecuteHook hook, object trunk = null ) {
			Item item = new Item( name, hook, trunk );

			int Spacing = 30 * ( list.Parent.Container.Items.Count );

			item.Append( new UIContainer( new Point( 0, Spacing ), list.Root().Container.Size.H( 30 ), Color.FromArgb( 35, 0, 0, 0 ) ) );
			item.Append( new UIText( item.sName, new Point( list.Root().Container.Size.H( 0 ).Divide() ), 0.6f, Color.White, GTA.Font.Monospace, true ) );
			list.Append( ref item );

			return list;
		}

		public static Size Divide( this Size size, int factor = 2 ) {
			return new Size( size.Width / factor, size.Height / factor );
		}

		public static Size W( this Size size, int width ) {
			return new Size( size.Height, width );
		}

		public static Size H( this Size size, int height ) {
			return new Size( size.Width, height );
		}
	}
}

