﻿using System;
using System.Windows.Forms;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using iDirector.Core;
using GTA;
using iDirector.Client.UI.Widgets;

namespace iDirector.Client.UI
{
	public static class Handler
	{
		public static Admin Admin = new Admin();

		public static void Load() {
			
		}

		public static void onTick( object sender, EventArgs e ) {
			if(Admin.bEnabled){
				Admin.stateCurrent.HookTick();
				Admin.Draw();

			}
			
		}

		public static void onKeyUp( KeyEventArgs e ) {
			if( Input.isHotkeyPressed( "MenuToggle", e ) ) Admin.Toggle();
			if( !Admin.bEnabled ) return;
			if( Input.isHotkeyPressed( "MenuUp", e ) ) Admin.Up();
			if( Input.isHotkeyPressed( "MenuDown", e ) ) Admin.Down();
			if( Input.isHotkeyPressed( "MenuSelect", e ) ) Admin.Select();
			if( Input.isHotkeyPressed( "MenuBack", e ) ) Admin.Back();

		}
	}
	
}
