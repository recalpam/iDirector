﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.Windows.Forms;
using GTA;
using GTA.Native;
using iDirector.Core;

namespace iDirector.Client.UI
{
	// My framework's UI is build from multiple 'Widget' -s
	// They are the root of every UI Module/Component.
	public abstract class Widget:Element
	{
		protected List<State> States = new List<State>();
		protected List<State> statesPrevious = new List<State>();
		public State stateCurrent;

		public void stateActivate( string name ) {
			State result;
			if( stateByName( name, out result ) ) {
				statesPrevious.Add( stateCurrent );
				stateCurrent = ( State )result;
				if( stateCurrent.List != null ) stateCurrent.List.Refresh();
			}
		}

		public bool stateByName( string name, out State stateResult ) {
			stateResult = new State( "dummy" );
			foreach( State state in States ) {
				if( state.sName == name ) {
					stateResult = state;
					return true;
				}
			} return false;
		}

		public void stateActivatePrevious() {
			State last = statesPrevious.Last();
			stateCurrent = last;
			statesPrevious.RemoveAt( statesPrevious.LastIndexOf( last ) );
		}


		public void Append( State state ) {
			States.Add( state );
			state.Parent = this;
		}



	}

	// Widget are controlled by one or multiple 'State' -s
	// Every widget needs to have atleast one State.
	// A state determines which Items get rendered and so forth.
	// It also has its own tick function. This opens alot of options, amongst which are dynamic menu options with their own function payload.
	public class State:Element
	{
		public List<Element> Elements = new List<Element>();
		public EventArgs e = null;

		public event TickHandler Tick;
		public delegate void TickHandler( object sender, EventArgs e );

		//@todo Improve
		public List List;

		public State( string name ) : base( name ) { }

		public void Append( ref Element element ) {
			Elements.Add( element );
			base.Append( ref element );
		}

		public void HookTick() {
			if( Tick != null ) {
				Tick( this, e );
			}
		}

	}

	// A list of Items, which are selectable.
	// Used for selection screens, menu's, etc.
	public class List:Element
	{
		public List<Item> itemAll = new List<Item>();
		public Item itemCurrent;

		public event itemChange eItemChange;

		public delegate void itemChange( Item itemCurrent, Item itemNext );


		public List( string name ) : base( name ) { }

		public List( string name, itemChange hook )
			: base( name ) {
			eItemChange += hook;
		}

		public void Append( ref Item item ) {
			itemAll.Add( item );
			base.Append( ref item );
		}

		public void Refresh() {
			if( itemAll.Count > 0 ) {
				itemCurrent = itemAll.FirstOrDefault();
				if( eItemChange != null ) {
					eItemChange( itemCurrent, itemCurrent );
				}
			}
		}

		public override void Clear() {
			this.itemAll.Clear();
			base.Clear();
		}

		public void itemNext() {
			Item Next;

			if( itemAll.IndexOf( itemAll.Last() ) == itemAll.IndexOf( itemCurrent ) ) {
				//Next = itemCurrent = itemAll.FirstOrDefault();
			} else {
				Next = itemAll[itemAll.IndexOf( itemCurrent ) + 1];
				if( eItemChange != null ) eItemChange( itemCurrent, Next );

				itemCurrent = Next;
			}


		}

		public void itemPrev() {
			Item Prev;

			if( itemAll.IndexOf( itemAll.First() ) == itemAll.IndexOf( itemCurrent ) ) {
				//Prev = itemCurrent = itemAll.Last();
			} else {
				Prev = itemAll[itemAll.IndexOf( itemCurrent ) - 1];
				if( eItemChange != null ) eItemChange( itemCurrent, Prev );

				itemCurrent = Prev;
			}


		}


	}

	// Items that can be used in a List.
	// They can execute functionionality and are interactive.
	// They can also respond to focus (such as a glow when selected).
	public class Item:Element
	{

		public event ExecuteHook eHook;
		public delegate void ExecuteHook( Item item );
		public object Trunk; // you can store any additional data here. you can access these when your hooked event function gets called. Good for saving player ID's, models, etc that you want to use in your function.


		public Item( string name )
			: base( name ) {

		}

		public Item( string name, ExecuteHook hook, object trunk = null )
			: base( name ) {
			eHook += hook;
			Trunk = trunk;
		}

		public void Execute() {
			if( eHook != null ) {
				eHook( this );
			}
		}
	}

	// All above explained elements extend Element.
	// It contains all neccisary, but bare minimum, members for the entire UI framework to work in harmony.
	abstract public class Element
	{
		public Element Parent;
		public string sName;
		public bool bEnabled = true;
		public UIContainer Container;

		public Element() {
			Setup();

			/** 
			 * UI Draw stuff goes here like
			 * 
			 * Container = new UIContainer( new Point( 10, 240 ), new Size( 200, 300 ), Color.FromArgb( 200, 237, 239, 241 ) );
			 * Container.Items.Add( new UIRectangle( new Point( 0, 0 ), new Size( 200, 30 ), Color.FromArgb( 255, 26, 188, 156 ) ) );
			 * 
			 **/
		}

		public Element( string name ) {
			sName = name;
			Setup();

			/** 
			 * UI Draw stuff goes here like
			 * 
			 * Container = new UIContainer( new Point( 10, 240 ), new Size( 200, 300 ), Color.FromArgb( 200, 237, 239, 241 ) );
			 * Container.Items.Add( new UIRectangle( new Point( 0, 0 ), new Size( 200, 30 ), Color.FromArgb( 255, 26, 188, 156 ) ) );
			 * 
			 **/
		}

		// Gets executed before all other constructor code.
		// Used for pre-initialization configs.
		virtual protected void Setup() {

		}

		// Hides or shows the element, depending on current value.
		virtual public void Toggle() {
			bEnabled = !bEnabled;
		}

		// Draw the Container member
		virtual public void Draw() {
			if( bEnabled ) Container.Draw();
		}

		// Append the UIContainer of the given Element to itemlist of our Container 
		// The Element will replace the Container If no container is added yet, for convenience
		virtual public void Append<T>( ref T element ) where T:Element {
			if( null == this.Container ) {
				if( null != this.Parent ) {
					this.Parent.Append( ref element );
				} else {
					this.Container = element.Container;
					element.Parent = this;

				}
			} else {
				if( null != element.Container ) this.Container.Items.Add( element.Container );
				element.Parent = this;
			}
		}

		// Get the most upper parent container that is linked via the Parent member.
		// Used for data sharing between UI widgets that are trivial to store elsewhere, like size of a rectangle for example.
		public Element Root() {
			if( this.Parent != null && this.Parent.Container != null ) {
				return this.Parent.Root();
			} else {
				return this;
			}
		}

		virtual public void Clear(){
			if( this.Container != null ) {
				this.Container.Items.Clear();
			} else if(this.Parent != null) {
				this.Parent.Clear();
			}
		}

		// Append the given UIContainer to itemlist of our Container 
		// The Element will replace the Container If no container is added yet, for convenience
		virtual public void Append( GTA.UIContainer element ) {
			if( null == this.Container ) {
				this.Container = element;
			} else {
				this.Container.Items.Add( element );
			}
		}

		virtual public void Append( GTA.UIRectangle element ) {
			if( null != this.Container )
				this.Container.Items.Add( element );
		}

		virtual public void Append( GTA.UIText element ) {
			if( null != this.Container )
				this.Container.Items.Add( element );
		}
	}
}
