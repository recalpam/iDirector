﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using iDirector.Client.UI;
using iDirector.Core;
using GTA;
using GTA.Native;
using GTA.Math;
using iDirector.Client;

namespace iDirector.Client.UI.Widgets
{
	public class Admin : Menu
	{

		protected override void Setup() {

			// ADMIN MENU
			this.sName = "iDirector";

			// ROOT
			this.State( "Root" )
				.List( "Options" )
					.Item( "Network", NetworkMenu )
					.Item( "NPC", NPCMenu )
					.Refresh();

			//
			this.State( "NPCMenu" )
				.List( "Options" )
					.Item( "Spawn Baddy", NPCSpawnBaddy )
					.Refresh();

			// NETWORK
			this.State( "NetworkMenu" )
				.List( "Options" )
					.Item( "Individual", NetworkPlayerList )
					.Item( "Serverwide", NetworkServerWide )
					.Item( "Settings", NetworkSettings );

			this.State( "NetworkPlayerList" )
				.List( "Options" );
		}

		public void NPCSpawnBaddy( Item item ) {
			Ped ped = World.CreatePed( PedHash.Marine02SMY, Game.Player.Character.GetOffsetInWorldCoords( new Vector3( 0, 5, 0 ) ) );
		}

		public void NPCMenu( Item item ) {
			this.stateActivate( "NPCMenu" );
		}

		public void NetworkMenu(Item item) {
			if( !Network.Data.bOnline ) return;
			this.stateActivate( "NetworkMenu" );
		}

		public void NetworkPlayerList( Item item ) {
			State state;

			// are we in the clear?
			if( !Network.Data.bOnline ) return;
			if( !stateByName( "NetworkPlayerList", out state ) ) return;
			
			// clear all previous players from the menu
			state.List.Clear();

			// loop all players
			foreach(Player player in Network.Data.Players){
				state.List.Item( player.Name, Teleport, player.Handle );
			}

			//state.List.Refresh();
			this.stateActivate( "NetworkPlayerList" );
		}

		public void NetworkServerWide( Item item ) {

		}

		public void NetworkSettings( Item item ) {

		}

		public void Teleport( Item item ) {
			Player player;
			if(!Network.bPlayer((int)item.Trunk, out player)) return;
			Game.Player.Character.Position = player.Character.Position;
		}
	}
}
