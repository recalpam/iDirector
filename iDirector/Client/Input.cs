﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.Windows.Forms;
using GTA;
using GTA.Native;
using iDirector.Core;

namespace iDirector.Client
{
	static class Input
	{
		public static void onKeyDown( object sender, KeyEventArgs e ) {
			
		}

		public static void onKeyUp( object sender, KeyEventArgs e ) {
			UI.Handler.onKeyUp( e );
		}

		public static bool isHotkeyPressed( string name, KeyEventArgs e ) {
			if( int.Parse( Settings.xHotkeys.Element( name ).Value, System.Globalization.NumberStyles.HexNumber ) != e.KeyCode.GetHashCode() ) return false;
			Debug.Log( "Hotkey pressed: " + name, Debug.Type.Debug );
			return true;
		}
	}
}
