﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace iDirector.UI.Elements.AdminMenu
{
	class Main : Menu
	{
		public override void Setup() {
			base.Setup();
			Container = new UIContainer(new Point(10, 240), new Size(200, 300), Color.FromArgb(200, 237, 239, 241));
			Container.Items.Add(new UIRectangle(new Point(0, 0), new Size(200, 30), Color.FromArgb(255, 26, 188, 156)));
		}
	}
}
